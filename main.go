package main

import ( // Packages
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

const version string = "v1"
const speciesRequestString string = "http://api.gbif.org/v1/species/"
const countryRequestString string = "http://api.gbif.org/v1/occurrence/search?country="
const testRequest1 string = "http://api.gbif.org/v1/species/5787161"
const testRequest2 string = "http://restcountries.eu/rest/v2/alpha/VA"

var timeCounter time.Time // Declares a time variable

//----------------------------------------------------------------------------------------------
//										Structs
//----------------------------------------------------------------------------------------------

type Country struct { // Country data
	Name    string `json:"name"`
	Capital string `json:"capital"`
	Flag    string `json:"flag"`
}

type SpeciesInCountry struct { // Species occurence data
	SpeciesKey       int    `json:"speciesKey"`
	AcceptedTaxonKey int    `json:"acceptedTaxonKey"`
	ScientificName   string `json:"scientificName"`
}

type ListOfSpeciesInCountry struct { // Contains several of the above struct
	Results []SpeciesInCountry `json:"results"`
}

type OutCountry struct { // Delivery formated struct
	Name       string   `json:"name"`
	Capital    string   `json:"capital"`
	Flag       string   `json:"flag"`
	Species    []string `json:"species"`
	SpeciesKey []int    `json:"speciesKey"`
}

type Species struct { // Species data and delivery struct
	Key            int    `json:"key"`
	Kingdom        string `json:"kingdom"`
	Phylum         string `json:"phylum"`
	Order          string `json:"order"`
	Family         string `json:"family"`
	Genus          string `json:"genus"`
	ScientificName string `json:"scientificName"`
	CanonicalName  string `json:"canonicalName"`
	Year           string `json:"year"`
}

type Diag struct { // Diagnostics data and delivery struct
	Gbif          string `json:"gbif"`
	Restcountries string `json:"restcountries"`
	Version       string `json:"version"`
	Uptime        string `json:"string"`
}

//----------------------------------------------------------------------------------------------
//									Handler Functions
//----------------------------------------------------------------------------------------------

func HandlerSpecies(w http.ResponseWriter, re *http.Request) { // Handelefunc for species data

	var output Species // Initiate structs
	var year Species

	parts := strings.Split(re.URL.Path, "/") // Find species Key
	specKey := parts[2]

	res, e := http.Get(speciesRequestString + specKey) // Requests data from api
	r(e, w)                                            //error handling

	e = json.NewDecoder(res.Body).Decode(&output) // Makes data useful
	r(e, w)

	res, e = http.Get(speciesRequestString + specKey + "/name") // To get the year data
	r(e, w)

	defer res.Body.Close() // Avoid data leak

	e = json.NewDecoder(res.Body).Decode(&year) // Decode
	r(e, w)

	output.Year = year.Year // Add year

	var buffer = new(bytes.Buffer)     // Creates a binary storage
	encoder := json.NewEncoder(buffer) // Encode into buffer
	encoder.Encode(output)

	w.Header().Add("Content-Type", "application/json") // Beautifying
	w.WriteHeader(http.StatusOK)

	r(e, w)

	io.Copy(w, buffer) // Sends to server
}

func HandlerOccurrence(w http.ResponseWriter, re *http.Request) { // Handlefunc for country data

	var requestString string // Pre-declaration of string

	var speciesData ListOfSpeciesInCountry // Pre-declaration of structs
	var formatingStorage ListOfSpeciesInCountry
	var countryData Country
	var output OutCountry

	limit := re.FormValue("limit") // Value of limit

	parts := strings.Split(re.URL.Path, "/") // Finds countrycode
	countryCd := parts[2]

	if limit != "" { //	Makes the get adress
		requestString = countryRequestString + countryCd + "&limit=" + limit
	} else { // with no limit
		requestString = countryRequestString + countryCd
	}

	res, er := http.Get(requestString) // Gets country data
	r(er, w)

	er = json.NewDecoder(res.Body).Decode(&speciesData) // Makes useable
	r(er, w)

	for i := 0; i < len(speciesData.Results); i++ { //for each value in speciesData
		count := 0       // Counts where j starts
		inArray := false // if repeated

		for j := count - 1; j > 0; j-- { // for each value in formatingStorage
			if speciesData.Results[i] == formatingStorage.Results[j] { // If value is repeated
				inArray = true
			}
		}
		if !inArray { // Add if not
			formatingStorage.Results = append(formatingStorage.Results, speciesData.Results[i])
			count++ // Value added
		}
	}

	res, er = http.Get("http://restcountries.eu/rest/v2/alpha/" + countryCd) // Get country data
	r(er, w)

	er = json.NewDecoder(res.Body).Decode(&countryData) // Decodes
	r(er, w)
	defer res.Body.Close() // Avoid leak

	output.Name = countryData.Name // Insert data into output
	output.Capital = countryData.Capital
	output.Flag = countryData.Flag

	for i := 0; i < len(formatingStorage.Results); i++ { // for each in formating storage

		output.Species = append(output.Species, formatingStorage.Results[i].ScientificName) // Insert data into output

		if formatingStorage.Results[i].SpeciesKey != 0 { // Insert species key if not null
			output.SpeciesKey = append(output.SpeciesKey, formatingStorage.Results[i].SpeciesKey)
		} else { // If null insert lineage key
			output.SpeciesKey = append(output.SpeciesKey, formatingStorage.Results[i].AcceptedTaxonKey)
		}
	}

	var buffer = new(bytes.Buffer) // Binary storage

	encoder := json.NewEncoder(buffer) // Encodes into buffer
	encoder.Encode(output)

	w.Header().Add("Content-Type", "application/json") // Beautification
	w.WriteHeader(http.StatusOK)

	io.Copy(w, buffer) // Outputs to server

}

func HandlerDiag(w http.ResponseWriter, re *http.Request) { // Handlefunc for diag

	var diag Diag // Declares a diag struct

	res, er := http.Get(testRequest1) // Get data
	r(er, w)

	diag.Gbif = res.Status //  GBIF status

	res, er = http.Get(testRequest2) // Get data
	r(er, w)
	defer res.Body.Close() // avoid leak

	diag.Restcountries = res.Status // Restcountry status

	diag.Version = version // Sets version and calculates time
	diag.Uptime = time.Since(timeCounter).String()

	var buffer = new(bytes.Buffer) // Binary storage

	encoder := json.NewEncoder(buffer) // Encodes into buffer
	encoder.Encode(diag)

	w.Header().Add("Content-Type", "application/json") //Beautification
	w.WriteHeader(http.StatusOK)

	io.Copy(w, buffer) // Outputs to server
}

func HandleBadRequest(w http.ResponseWriter, re *http.Request) { // Url error handling
	http.Error(w, "Bad Request", http.StatusBadRequest)
	fmt.Fprintf(w, "Usage:\n/country/countrycode\n/species/specieskey\n/diag")
}

//----------------------------------------------------------------------------------------------
//									Other Functions
//----------------------------------------------------------------------------------------------

func r(r error, w http.ResponseWriter) {
	if r != nil { //  Checks for errors
		log.Print(r)
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
}

//----------------------------------------------------------------------------------------------
//										Main
//----------------------------------------------------------------------------------------------

func main() {
	timeCounter = time.Now() // Stores start time

	port := os.Getenv("PORT") // Finds port, if none sets to 8080
	if port == "" {
		port = "8080"
		fmt.Println("Port not found! Setting to 8080")
	}

	http.HandleFunc("/", HandleBadRequest) // Handlefuncs
	http.HandleFunc("/species/", HandlerSpecies)
	http.HandleFunc("/country/", HandlerOccurrence)
	http.HandleFunc("/diag/", HandlerDiag)

	log.Fatal(http.ListenAndServe(":"+port, nil)) // Starts server
}
